import time
from diploma.rdf_controller import RdfController
from diploma.tidy_tree import add_same_as, collect_node_uris, add_enriched_same_as


if __name__ == '__main__':
    rdf = RdfController("bolt://localhost:7687", "neo4j", "test")

    print("This was a triumph")

    target_uri = "http://wikidata.dbpedia.org/resource/Q1458083"

    tree = rdf.get_children_path_by_uri(target_uri)

    tree_uris = collect_node_uris(tree)

    t0 = time.time()
    same_as_with_children = rdf.get_same_as_and_children_of_same_as_many(tree_uris)
    add_same_as(same_as_with_children, tree)
    t1 = time.time()
    print(t1 - t0)

    enriched_same_as_with_children = rdf.get_enriched_same_as_and_children_of_enriched_same_as_many(tree_uris)
    add_enriched_same_as(enriched_same_as_with_children, tree)

    t2 = time.time()
    print(t2 - t1)
