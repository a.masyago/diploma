import React, { useEffect, useState, useCallback } from 'react';
import Breadcrumb from 'react-bootstrap/Breadcrumb';
import Button from 'react-bootstrap/Button';
import Container from 'react-bootstrap/Container';
import Dropdown from 'react-bootstrap/Dropdown';
import Form from 'react-bootstrap/Form';
import FormControl from 'react-bootstrap/FormControl';
import Modal from 'react-bootstrap/Modal';
import Navbar from 'react-bootstrap/Navbar';
import Tab from 'react-bootstrap/Tab';
import Tabs from 'react-bootstrap/Tabs';

import DisplayOptions from './DisplayOptions';
import SunburstChart from './SunburstChart';
import TidyTree from './TidyTree';

import {
  getTidyTree,
  getRelationTypes,
  searchConcept,
  createNewRelation
} from './api';

import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css';

function App() {
  const [relationTypes, setRelationTypes] = useState([{'name': 'none', 'relation': ''}]);
  const [relations, setRelations] = useState(['', '']);
  const [search, setSearch] = useState('Science')
  const [searchResults, setSearchResults] = useState([]);
  const [searchResultsVisible, setSearchResultsVisible] = useState(false);

  const [loading, setLoading] = useState(false);
  const [tidyTreeData, setTidyTreeData] = useState({});
  const [subjects, setSubjects] = useState({});
  const [showSubjects, setShowSubjects] = useState(false);
  const [history, setHistory] = useState([{uri: 'http://wikidata.dbpedia.org/resource/Q1458083', name: 'Science'}]);

  useEffect(() => {
    getRelationTypes().then(response => {
      setRelationTypes([{'name': 'None', 'relation': ''}, ...response])
      if (response.length > 1) {
        setRelations([response[0].relation, response[1].relation]);
      } else if (response.length === 1) {
        setRelations([response[0].relation, '']);
      } else {
        setRelations(['', '']);
      }
    })
  }, [setRelationTypes]);

  const onSearch = useCallback(() => {
    searchConcept(search).then((data) => {
      setSearchResults(data);
      if (data.length) {
        setSearchResultsVisible(true);
      }
    })
  }, [search]);

  const onNewRoot = useCallback((uri, name) => {
    setSearchResultsVisible(false);
    setSearchResults([]);
    setSearch('');
    setHistory([{uri, name}]);
  }, []);

  const onTextClick = useCallback(({uri, name}) => {
    setHistory([...history, {uri, name}]);
  }, [history]);

  const onNodeClick = useCallback(data => {
    setSubjects(data.subjects);
    setShowSubjects(true);
  }, []);

  const closeSubjects = useCallback(() => setShowSubjects(false), []);

  useEffect(() => {
    const uri = history[history.length - 1].uri;

    setLoading(true);
    getTidyTree(uri, relations).then(setTidyTreeData).then(() => {
      setLoading(false);
    });
  }, [history, relations]);

  const onNewRelation = (data) => createNewRelation(data).then((response) =>
    setRelationTypes([{'name': 'None', 'relation': ''}, ...response])
  );

  return (
    <div className="App">
      <Container>
        <Navbar bg="light" expand="lg">
          <Navbar.Brand href="#home">Graph interlinks visualization</Navbar.Brand>
          <Dropdown>
            <Form inline className="ml-auto mr-auto">
              <FormControl type="text" value={search} onChange={(e) => setSearch(e.target.value)} placeholder="Search" className="mr-sm-2"  />
              <Button variant="outline-success" type="button" onClick={onSearch}>Search</Button>
            </Form>

            <Dropdown.Menu show={searchResultsVisible}>
            {searchResults.map(r =>
              <Dropdown.Item key={r.id} onClick={() => onNewRoot(r.uri, r.name)}>
                {r.name}
              </Dropdown.Item>
            )}
            </Dropdown.Menu>
          </Dropdown>
        </Navbar>

        <DisplayOptions
          relations={relations}
          relationTypes={relationTypes}
          setRelations={setRelations}
          onNewRelation={onNewRelation}
          loading={loading}
        />

        <Tabs defaultActiveKey="tidy-tree">
          <Tab eventKey="tidy-tree" title="Tidy tree">
            <div style={{ height: 100, textAlign: 'left', padding: '20px 40px 20px 40px' }}>
              <Breadcrumb>
                {history.map((historyItem, i) =>
                  (i < history.length - 1)
                  ?
                  (<Breadcrumb.Item key={historyItem.uri} href="#" onClick={() => setHistory(history.slice(0, i + 1))}>{historyItem.name}</Breadcrumb.Item>)
                  :
                  (<Breadcrumb.Item key={historyItem.uri} active>{historyItem.name}</Breadcrumb.Item>)
                )}
              </Breadcrumb>
            </div>

            {(!!loading) &&
              <div>Loading...</div>
            }

            {(!loading) &&
              <TidyTree data={tidyTreeData} onNodeClick={onNodeClick} onTextClick={onTextClick} />
            }
          </Tab>

          <Tab eventKey="sunburst" title="Sunburst">
            <div style={{ height: 100, textAlign: 'left', padding: '20px 40px 20px 40px' }}>
              <Breadcrumb>
                {history.map((historyItem, i) =>
                  (i < history.length - 1)
                  ?
                  (<Breadcrumb.Item key={historyItem.uri} href="#" onClick={() => setHistory(history.slice(0, i + 1))}>{historyItem.name}</Breadcrumb.Item>)
                  :
                  (<Breadcrumb.Item key={historyItem.uri} active>{historyItem.name}</Breadcrumb.Item>)
                )}
              </Breadcrumb>
            </div>

            {(!!loading) &&
              <div>Loading...</div>
            }

            {(!loading) &&
              <SunburstChart data={tidyTreeData} onTextClick={onTextClick} />
            }
          </Tab>
        </Tabs>

        <Modal show={showSubjects} onHide={closeSubjects} animation={false} size="lg" className="modal-dialog-custom">
          <Modal.Header closeButton>
            <Modal.Title>Subjects</Modal.Title>
          </Modal.Header>

          <Modal.Body>
            <TidyTree data={subjects} width={600} />
          </Modal.Body>

          <Modal.Footer>
            <Button variant="secondary" onClick={closeSubjects}>Close</Button>
          </Modal.Footer>
        </Modal>

      </Container>
    </div>
  );
}

export default App;
