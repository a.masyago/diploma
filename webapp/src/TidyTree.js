import React from 'react';
import { tree as d3tree, hierarchy, linkHorizontal, select } from 'd3';

// Config
const C = {
  DEFAULT_WIDTH: 1000,
  NODE_DX: 45,
  TARGET_COLOR: '#1da6db',
  SOURCE_COLOR: '#db1dcf',
  ENRICHED_COLOR: '#f48f0d',
  STROKE_WIDTH: 1.0,
  TEXT_X_OFFSET: 20,
  TEXT_Y_2_OFFSET: 6,
  TEXT_Y_3_OFFSET: 12,
  CIRCLE_R: 10,
  STATISTICS_R: 20
};


export default class TidyTree extends React.PureComponent {
  constructor(props) {
    super(props);

    this.chart = React.createRef();
  };

  countSubjectsStatistics = (data) => {
    const newData = { ...data };

    if (!!newData.subjects && !!newData.subjects.children && newData.subjects.children.length) {
      newData.numTargets = 0;
      newData.numSources = 0;
      newData.numBoth = 0;
      newData.subjects.children.forEach((ch => {
        if (!!ch.name && !!ch.sameAsName) {
          newData.numBoth += 1;
        } else if (!!ch.name) {
          newData.numTargets += 1;
        } else if (!!ch.sameAsName) {
          newData.numSources += 1;
        }
      }));

      newData.numTotal = newData.numTargets + newData.numSources + newData.numBoth;
    }

    if (newData.children) {
      newData.children = newData.children.map((ch) => this.countSubjectsStatistics(ch));
    }

    return newData;
  };

  makeTree = (data, width) => {
    const root = hierarchy(data);
    root.dx = C.NODE_DX;
    root.dy = width / (root.height + 1);
    return d3tree().nodeSize([root.dx, root.dy])(root);
  };

  renderCircles = (node) => {
    const R = C.CIRCLE_R;

    const onClick = (event, d) => {
      if (this.props.onNodeClick && d.data.subjects) {
        this.props.onNodeClick(d.data);
      }
    };

    // Single name
    node.select(function(d) { return (!!d.data.name && !d.data.sameAsName && !d.data.enrichedSameAsName) ? this : null })
      .append('path')
        .attr('d', function(d) {
          return `M 0 -${R} A ${R} ${R} 0 0 1 0 ${R} A ${R} ${R} 0 0 1 0 -${R}`;
        })
        .attr('fill', C.TARGET_COLOR)
        .attr('class', 'target-circle')
        .on("click", onClick);

    node.select(function(d) { return (!d.data.name && !!d.data.sameAsName && !d.data.enrichedSameAsName) ? this : null })
      .append('path')
        .attr('d', function(d) {
          return `M 0 -${R} A ${R} ${R} 0 0 1 0 ${R} A ${R} ${R} 0 0 1 0 -${R}`;
        })
        .attr('fill', C.SOURCE_COLOR);

    node.select(function(d) { return (!d.data.name && !d.data.sameAsName && !!d.data.enrichedSameAsName) ? this : null })
      .append('path')
        .attr('d', function(d) {
          return `M 0 -${R} A ${R} ${R} 0 0 1 0 ${R} A ${R} ${R} 0 0 1 0 -${R}`;
        })
        .attr('fill', C.ENRICHED_COLOR);

    // Two names
    node.select(function(d) { return (!!d.data.name && !!d.data.sameAsName && !d.data.enrichedSameAsName) ? this : null })
      .append('path')
        .attr('d', function(d) {
          return `M 0 ${R} A ${R} ${R} 0 0 1 0 -${R} L 0 0`;
        })
        .attr('fill', C.TARGET_COLOR)
        .attr('class', 'target-circle')
        .on("click", onClick);
    node.select(function(d) { return (!!d.data.name && !!d.data.sameAsName && !d.data.enrichedSameAsName) ? this : null })
      .append('path')
        .attr('d', function(d) {
          return `M 0 -${R} A ${R} ${R} 0 0 1 0 ${R} L 0 0`;
        })
        .attr('fill', C.SOURCE_COLOR)
        .attr('class', 'target-circle')
        .on("click", onClick);

    node.select(function(d) { return (!!d.data.name && !d.data.sameAsName && !!d.data.enrichedSameAsName) ? this : null })
      .append('path')
        .attr('d', function(d) {
          return `M 0 ${R} A ${R} ${R} 0 0 1 0 -${R} L 0 0`;
        })
        .attr('fill', C.TARGET_COLOR)
        .attr('class', 'target-circle')
        .on("click", onClick);
    node.select(function(d) { return (!!d.data.name && !d.data.sameAsName && !!d.data.enrichedSameAsName) ? this : null })
      .append('path')
        .attr('d', function(d) {
          return `M 0 -${R} A ${R} ${R} 0 0 1 0 ${R} L 0 0`;
        })
        .attr('fill', C.ENRICHED_COLOR)
        .attr('class', 'target-circle')
        .on("click", onClick);

    node.select(function(d) { return (!d.data.name && !!d.data.sameAsName && !!d.data.enrichedSameAsName) ? this : null })
      .append('path')
        .attr('d', function(d) {
          return `M 0 ${R} A ${R} ${R} 0 0 1 0 -${R} L 0 0`;
        })
        .attr('fill', C.SOURCE_COLOR);
    node.select(function(d) { return (!d.data.name && !!d.data.sameAsName && !!d.data.enrichedSameAsName) ? this : null })
      .append('path')
        .attr('d', function(d) {
          return `M 0 -${R} A ${R} ${R} 0 0 1 0 ${R} L 0 0`;
        })
        .attr('fill', C.ENRICHED_COLOR);

    // All three names
    const dx = (4.330127018922194 / 5) * R;
    const dy = (2.4999999999999996 / 5) * R;
    node.select(function(d) { return (!!d.data.name && !!d.data.sameAsName && !!d.data.enrichedSameAsName) ? this : null })
      .append('path')
        .attr('d', function(d) {
          return `M 0 -${R} A ${R} ${R} 0 0 1 ${dx} ${dy} L 0 0`;
        })
        .attr('fill', C.TARGET_COLOR)
        .attr('class', 'target-circle')
        .on("click", onClick);
    node.select(function(d) { return (!!d.data.name && !!d.data.sameAsName && !!d.data.enrichedSameAsName) ? this : null })
      .append('path')
        .attr('d', function(d) {
          return `M ${dx} ${dy} A ${R} ${R} 0 0 1 -${dx} ${dy} L 0 0`;
        })
        .attr('fill', C.SOURCE_COLOR)
        .attr('class', 'target-circle')
        .on("click", onClick);
    node.select(function(d) { return (!!d.data.name && !!d.data.sameAsName && !!d.data.enrichedSameAsName) ? this : null })
      .append('path')
        .attr('d', function(d) {
          return `M -${dx} ${dy} A ${R} ${R} 0 0 1 0 -${R} L 0 0`;
        })
        .attr('fill', C.ENRICHED_COLOR)
        .attr('class', 'target-circle')
        .on("click", onClick);
  }

  polarToCartesian = (centerX, centerY, radius, angleInDegrees) => {
    var angleInRadians = (angleInDegrees - 90) * Math.PI / 180.0;

    return {
      x: centerX + (radius * Math.cos(angleInRadians)),
      y: centerY + (radius * Math.sin(angleInRadians))
    };
  }

  renderStatistics = (node) => {
    const R = C.CIRCLE_R + 2;
    const R2 = C.STATISTICS_R;
    const R3 = (R + R2) / 2;
    const converter = this.polarToCartesian;

    node.select(function(d) { return !!d.data.numTargets ? this : null })
      .append('path')
        .attr('d', function(d) {
          let la = 0;
          if (d.data.numTargets * 2 > d.data.numTotal) {
            la = 1;
          }

          const { x: dxInner, y: dyInner } = converter(0, 0, R, (d.data.numTargets / d.data.numTotal) * 360);
          const { x: dxOuter, y: dyOuter } = converter(0, 0, R2, (d.data.numTargets / d.data.numTotal) * 360);
          return `M 0 -${R} A ${R} ${R} 0 ${la} 1 ${dxInner} ${dyInner} L ${dxOuter} ${dyOuter} A ${R2} ${R2} 0 ${la} 0 0 -${R2} L 0 -${R}`;
        })
        .attr('fill', C.TARGET_COLOR)
        .attr('class', 'target-statistic-circle');

    node.select(function(d) { return !!d.data.numSources ? this : null })
      .append('path')
        .attr('d', function(d) {
          let la = 0;
          if (d.data.numSources * 2 > d.data.numTotal) {
            la = 1;
          }

          const { x: dxInner, y: dyInner } = converter(0, 0, R, (d.data.numSources / d.data.numTotal) * 360);
          const { x: dxOuter, y: dyOuter } = converter(0, 0, R2, (d.data.numSources / d.data.numTotal) * 360);
          return `M 0 -${R} A ${R} ${R} 0 ${la} 0 ${-dxInner} ${dyInner} L ${-dxOuter} ${dyOuter} A ${R2} ${R2} 0 ${la} 1 0 -${R2} L 0 -${R}`;
        })
        .attr('fill', C.SOURCE_COLOR)
        .attr('class', 'source-statistic-circle');

    node.select(function(d) { return !!d.data.numBoth ? this : null })
      .append('path')
        .attr('d', function(d) {
          let la = 0;
          if (d.data.numBoth * 2 > d.data.numTotal) {
            la = 1;
          }

          const { x: dxInnerStart, y: dyInnerStart } = converter(0, 0, R, ((d.data.numTargets || 0) / d.data.numTotal) * 360);
          const { x: dxInnerEnd, y: dyInnerEnd } = converter(0, 0, R, ((d.data.numSources || 0) / d.data.numTotal) * 360);
          const { x: dxOuterStart, y: dyOuterStart } = converter(0, 0, R3, ((d.data.numTargets || 0) / d.data.numTotal) * 360);
          const { x: dxOuterEnd, y: dyOuterEnd } = converter(0, 0, R3, ((d.data.numSources || 0) / d.data.numTotal) * 360);

          return `M ${dxInnerStart} ${dyInnerStart} A ${R} ${R} 0 ${la} 1 ${-dxInnerEnd} ${dyInnerEnd} L ${-dxOuterEnd} ${dyOuterEnd} A ${R3} ${R3} 0 ${la} 0 ${dxOuterStart} ${dyOuterStart} L ${dxInnerStart} ${dyInnerStart}`;
        })
        .attr('fill', C.TARGET_COLOR)
        .attr('class', 'target-source-statistic-circle');

    node.select(function(d) { return !!d.data.numBoth ? this : null })
      .append('path')
        .attr('d', function(d) {
          let la = 0;
          if (d.data.numBoth * 2 > d.data.numTotal) {
            la = 1;
          }

          const { x: dxInnerStart, y: dyInnerStart } = converter(0, 0, R3, ((d.data.numTargets || 0) / d.data.numTotal) * 360);
          const { x: dxInnerEnd, y: dyInnerEnd } = converter(0, 0, R3, ((d.data.numSources || 0) / d.data.numTotal) * 360);
          const { x: dxOuterStart, y: dyOuterStart } = converter(0, 0, R2, ((d.data.numTargets || 0) / d.data.numTotal) * 360);
          const { x: dxOuterEnd, y: dyOuterEnd } = converter(0, 0, R2, ((d.data.numSources || 0) / d.data.numTotal) * 360);

          return `M ${dxInnerStart} ${dyInnerStart} A ${R3} ${R3} 0 ${la} 1 ${-dxInnerEnd} ${dyInnerEnd} L ${-dxOuterEnd} ${dyOuterEnd} A ${R2} ${R2} 0 ${la} 0 ${dxOuterStart} ${dyOuterStart} L ${dxInnerStart} ${dyInnerStart}`;
        })
        .attr('fill', C.SOURCE_COLOR)
        .attr('class', 'target-source-statistic-circle');
  }

  renderText = (node) => {
    const onClick = (event, d) => {
      if (this.props.onTextClick && d.data.type === 'target') {
        this.props.onTextClick(d.data);
      }
    };

    const xOffset = (d) => {
      let offset = C.TEXT_X_OFFSET;
      if (d.data.numTotal) {
        offset += C.STATISTICS_R - C.CIRCLE_R;
      }

      if (d.data.children) {
        offset = -1 * offset;
      }

      return offset;
    }

    node.select(function(d) { return (!!d.data.name) ? this : null })
      .append('text')
        .attr('dy', '0.32em')
        .attr('x', xOffset)
        .attr('y', d => {
          if (!!d.data.sameAsName && !!d.data.enrichedSameAsName) {
            return -1 * C.TEXT_Y_3_OFFSET;
          } else if (!!d.data.sameAsName || !!d.data.enrichedSameAsName) {
            return -1 * C.TEXT_Y_2_OFFSET;
          } else {
            return 0;
          }
        })
        .attr('fill', C.TARGET_COLOR)
        .attr('text-anchor', d => d.children ? 'end' : 'start')
        .attr('class', 'target-text')
        .text(d => d.data.name)
        .on("click", onClick);

    node.select(function(d) { return (!!d.data.sameAsName) ? this : null })
      .append('text')
        .attr('dy', '0.32em')
        .attr('x', xOffset)
        .attr('y', d => {
          if (!!d.data.name && !!d.data.enrichedSameAsName) {
            return 0;
          } else if (!!d.data.name) {
            return C.TEXT_Y_2_OFFSET;
          } else if (!!d.data.enrichedSameAsName) {
            return -1 * C.TEXT_Y_2_OFFSET;
          } else {
            return 0;
          }
        })
        .attr('fill', C.SOURCE_COLOR)
        .attr('text-anchor', d => d.children ? 'end' : 'start')
        .text(d => d.data.sameAsName);

    node.select(function(d) { return (!!d.data.enrichedSameAsName) ? this : null })
      .append('text')
        .attr('dy', '0.32em')
        .attr('x', xOffset)
        .attr('y', d => {
          if (!!d.data.name && !!d.data.sameAsName) {
            return C.TEXT_Y_3_OFFSET;
          } else if (!!d.data.name || !!d.data.sameAsName) {
            return C.TEXT_Y_2_OFFSET;
          } else {
            return 0;
          }
        })
        .attr('fill', C.ENRICHED_COLOR)
        .attr('text-anchor', d => d.children ? 'end' : 'start')
        .text(d => d.data.enrichedSameAsName);
  };

  renderLines = (node, g) => {
    node.select(function(d) {
      return (!!d.data.name) ? this : null
    }).each(function (n, j) {
      const currentNode = select(this);
      const currentData = currentNode.data()[0];

      if (!n.children) {
        return;
      }

      n.children
        .filter(ch => !!ch.data.name)
        .forEach(ch => {
          const offsetStart = currentData.data.numTotal ? C.STATISTICS_R : C.CIRCLE_R;
          const offsetEnd = ch.data.numTotal ? C.STATISTICS_R : C.CIRCLE_R;

          g.append('path')
            .attr('d', linkHorizontal()
              .source(d => [currentData.y + offsetStart, currentData.x - C.STROKE_WIDTH * 2])
              .target(d => [ch.y - offsetEnd, ch.x - C.STROKE_WIDTH * 2])
            )
            .attr('fill', 'none')
            .attr('stroke', C.TARGET_COLOR)
            .attr('stroke-opacity', 0.4)
            .attr('stroke-width', C.STROKE_WIDTH);
        });

    });

    node.select(function(d) {
      return (!!d.data.sameAsName) ? this : null
    }).each(function (n, j) {
      const currentNode = select(this);
      const currentData = currentNode.data()[0];

      if (!n.children) {
        return;
      }

      n.children
        .filter(ch => !!ch.data.sameAsName && ch.data.sameAsParentIds && ch.data.sameAsParentIds.find(x => x === currentData.data.sameAsId))
        .forEach(ch => {
          const offsetStart = currentData.data.numTotal ? C.STATISTICS_R : C.CIRCLE_R;
          const offsetEnd = ch.data.numTotal ? C.STATISTICS_R : C.CIRCLE_R;

          g.append('path')
            .attr('d', linkHorizontal()
              .source(d => [currentData.y + offsetStart, currentData.x])
              .target(d => [ch.y - offsetEnd, ch.x])
            )
            .attr('fill', 'none')
            .attr('stroke', C.SOURCE_COLOR)
            .attr('stroke-opacity', 0.4)
            .attr('stroke-width', C.STROKE_WIDTH);
        });

    });

    node.select(function(d) {
      return (d.data.type === 'target' && !!d.data.children) ? this : null ;
    }).each(function (n, j) {
      const currentNode = select(this);
      const currentData = currentNode.data()[0];

      n.children
        .filter(ch => !!ch.data.enrichedSameAsName && ch.data.enrichedSameAsParentIds && ch.data.enrichedSameAsParentIds.find(x => x === currentData.data.enrichedSameAsId))
        .forEach(ch => {
          const offsetStart = currentData.data.numTotal ? C.STATISTICS_R : C.CIRCLE_R;
          const offsetEnd = ch.data.numTotal ? C.STATISTICS_R : C.CIRCLE_R;

          g.append('path')
            .attr('d', linkHorizontal()
              .source(d => [currentData.y + offsetStart, currentData.x + C.STROKE_WIDTH * 2])
              .target(d => [ch.y - offsetEnd, ch.x + C.STROKE_WIDTH * 2])
            )
            .attr('fill', 'none')
            .attr('stroke', C.ENRICHED_COLOR)
            .attr('stroke-opacity', 0.4)
            .attr('stroke-width', C.STROKE_WIDTH);
        });
    });
  }

  renderChart = (width=C.DEFAULT_WIDTH) => {
    if (!this.chart.current) {
      return;
    }

    const svg = select(this.chart.current);
    svg.selectAll("*").remove();

    const dataWithStats = this.countSubjectsStatistics(this.props.data);
    const root = this.makeTree(dataWithStats, this.props.width || width);

    let x0 = Infinity;
    let x1 = -x0;
    root.each(d => {
      if (d.x > x1) x1 = d.x;
      if (d.x < x0) x0 = d.x;
    });

    svg
      .attr('width', '100%')
      .attr('viewBox', [-30, 0, (this.props.width || width) + 30, x1 - x0 + root.dx * 2]);

    const g = svg
      .append('g')
        .attr('font-family', 'sans-serif')
        .attr('font-size', 10)
        .attr('transform', `translate(${root.dy / 3},${root.dx - x0})`);

    const node = g.append('g')
        .attr('stroke-linejoin', 'round')
        .attr('stroke-width', 3)
      .selectAll('g')
      .data(root.descendants())
      .join('g')
        .attr('transform', d => `translate(${d.y},${d.x})`);

    this.renderText(node);

    this.renderCircles(node);

    this.renderStatistics(node);

    this.renderLines(node, g);
  };

  componentDidMount = () => this.renderChart();
  componentDidUpdate = () => this.renderChart();

  render = () => (
    <div>
      <svg ref={this.chart} />
    </div>
  );
}
