export const searchConcept = (s) => fetch(`/api/search/?search=${s}`).then(r => r.json());
export const getTidyTree = (uri='', relations=['', '']) => fetch(`/api/tidy_tree/?uri=${uri}&relations=${relations.join(',')}`).then(r => r.json());
export const getRelationTypes = () => fetch('/api/relations/').then(r => r.json());
export const createNewRelation = (formData) => fetch('/api/new-relation/', {
  method: 'POST',
  mode: 'same-origin',
  cache: 'no-cache',
  body: formData
}).then(r => r.json());
