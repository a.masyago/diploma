import React from 'react';
import * as d3 from 'd3';


const C = {
  TARGET_COLOR: '#1da6db',
  SOURCE_COLOR: '#db1dcf',
  ENRICHED_COLOR: '#f48f0d'
};


export default class SunburstChart extends React.PureComponent {
  constructor(props) {
    super(props);
    this.width = 640;
    this.radius = this.width / 2;
    this.chart = React.createRef();
  };

  score = (data) => {
    let s = 0;
    if (data.name) {
      s += 1.2;
    }

    if (data.sameAsName) {
      s += 1.1;
    }

    if (data.enrichedSameAsName) {
      s += 1;
    }

    return s;
  }

  partition = (data) =>
    d3.partition().size([2 * Math.PI, this.radius * this.radius])(
      d3
        .hierarchy(data)
        .sum(d => !!d.children ? 0 : 1)
        .sort((a, b) => {
          return this.score(b.data) - this.score(a.data);
        })
    );

  tripleArc = (d, n, i) => d3
      .arc()
      .startAngle(d => d.x0)
      .endAngle(d => d.x1)
      .padAngle(1 / this.radius)
      .padRadius(this.radius)
      .innerRadius(d => Math.sqrt(d.y0) + (Math.sqrt(d.y1) - Math.sqrt(d.y0)) * i / n)
      .outerRadius(d => Math.sqrt(d.y0) + (Math.sqrt(d.y1) - Math.sqrt(d.y0)) * (i + 1) / n - (i === (n - 1) ? 1 : 0))(d);

  arc = () => d3
    .arc()
    .startAngle(d => d.x0)
    .endAngle(d => d.x1)
    .padAngle(1 / this.radius)
    .padRadius(this.radius)
    .innerRadius(d => Math.sqrt(d.y0))
    .outerRadius(d => Math.sqrt(d.y1) - 1);

  mousearc = () => d3
    .arc()
    .startAngle(d => d.x0)
    .endAngle(d => d.x1)
    .innerRadius(d => Math.sqrt(d.y0))
    .outerRadius(this.radius);

  drawArcs = (svg, root) => {
    svg
      .append("g")
      .selectAll("path")
      .data(root.descendants().filter(d => d.data.name && d.data.sameAsName && d.data.enrichedSameAsName))
      .join("path")
      .attr('fill', C.TARGET_COLOR)
      .attr("d", d => this.tripleArc(d, 3, 0));

    svg
      .append("g")
      .selectAll("path")
      .data(root.descendants().filter(d => d.data.name && d.data.sameAsName && d.data.enrichedSameAsName))
      .join("path")
      .attr('fill', C.SOURCE_COLOR)
      .attr("d", d => this.tripleArc(d, 3, 1));

    svg
      .append("g")
      .selectAll("path")
      .data(root.descendants().filter(d => d.data.name && d.data.sameAsName && d.data.enrichedSameAsName))
      .join("path")
      .attr('fill', C.ENRICHED_COLOR)
      .attr("d", d => this.tripleArc(d, 3, 2));


    svg
      .append("g")
      .selectAll("path")
      .data(root.descendants().filter(d => d.data.name && d.data.sameAsName && !d.data.enrichedSameAsName))
      .join("path")
      .attr('fill', C.TARGET_COLOR)
      .attr("d", d => this.tripleArc(d, 2, 0));

    svg
      .append("g")
      .selectAll("path")
      .data(root.descendants().filter(d => d.data.name && d.data.sameAsName && !d.data.enrichedSameAsName))
      .join("path")
      .attr('fill', C.SOURCE_COLOR)
      .attr("d", d => this.tripleArc(d, 2, 1));

    svg
      .append("g")
      .selectAll("path")
      .data(root.descendants().filter(d => d.data.name && !d.data.sameAsName && d.data.enrichedSameAsName))
      .join("path")
      .attr('fill', C.TARGET_COLOR)
      .attr("d", d => this.tripleArc(d, 2, 0));

    svg
      .append("g")
      .selectAll("path")
      .data(root.descendants().filter(d => d.data.name && !d.data.sameAsName && d.data.enrichedSameAsName))
      .join("path")
      .attr('fill', C.ENRICHED_COLOR)
      .attr("d", d => this.tripleArc(d, 2, 1));

    svg
      .append("g")
      .selectAll("path")
      .data(root.descendants().filter(d => !d.data.name && d.data.sameAsName && d.data.enrichedSameAsName))
      .join("path")
      .attr('fill', C.SOURCE_COLOR)
      .attr("d", d => this.tripleArc(d, 2, 0));

    svg
      .append("g")
      .selectAll("path")
      .data(root.descendants().filter(d => !d.data.name && d.data.sameAsName && d.data.enrichedSameAsName))
      .join("path")
      .attr('fill', C.ENRICHED_COLOR)
      .attr("d", d => this.tripleArc(d, 2, 1));

    svg
      .append("g")
      .selectAll("path")
      .data(root.descendants().filter(d => d.data.name && !d.data.sameAsName && !d.data.enrichedSameAsName))
      .join("path")
      .attr('fill', C.TARGET_COLOR)
      .attr("d", this.arc());

    svg
      .append("g")
      .selectAll("path")
      .data(root.descendants().filter(d => !d.data.name && d.data.sameAsName && !d.data.enrichedSameAsName))
      .join("path")
      .attr('fill', C.SOURCE_COLOR)
      .attr("d", this.arc());

    svg
      .append("g")
      .selectAll("path")
      .data(root.descendants().filter(d => !d.data.name && !d.data.sameAsName && d.data.enrichedSameAsName))
      .join("path")
      .attr('fill', C.ENRICHED_COLOR)
      .attr("d", this.arc());
  }

  renderChart = () => {
    if (!this.props.data) {
      return;
    }

    const oldSvg = d3.select(this.chart.current).select('svg');
    if (oldSvg) {
      oldSvg.remove();
    }

    const root = this.partition(this.props.data);
    const svg = d3.select(this.chart.current).append('svg');
    // Make this into a view, so that the currently hovered sequence is available to the breadcrumb
    const element = svg.node();

    element.value = { sequence: [], percentage: 0.0 };

    const label = svg
      .append("text")
      .attr("text-anchor", "middle")
      .attr("fill", "#444");

    label
      .append("tspan")
      .attr("class", "nodeName")
      .attr("x", 0)
      .attr("y", 0)
      .attr("dy", "-0.9em")
      .attr("font-size", "1em")
      .text(root.data.name);

    label
      .append("tspan")
      .attr("class", "nodeSameAsName")
      .attr("x", 0)
      .attr("y", 0)
      .attr("dy", "0")
      .attr("font-size", "1em")
      .text(root.data.sameAsName);

    label
      .append("tspan")
      .attr("class", "nodeEnrichedSameAsName")
      .attr("x", 0)
      .attr("y", 0)
      .attr("dy", "0.9em")
      .attr("font-size", "1em")
      .text(root.data.enrichedSameAsName);

    svg
      .attr("viewBox", `${-this.radius} ${-this.radius} ${this.width} ${this.width}`)
      .style("max-width", `${this.width}px`)
      .style("font", "12px sans-serif");

    this.drawArcs(svg, root);

    const path = svg.selectAll('path');

    svg
      .append("g")
      .attr("fill", "none")
      .attr("pointer-events", "all")
      .on("mouseleave", () => {
        path.attr("fill-opacity", 1);

        label
          .select(".nodeName")
          .text(root.data.name);

        label.select(".nodeSameAsName")
          .text(root.data.sameAsName);

        // Update the value of this view
        element.value = { sequence: [], percentage: 0.0 };
        element.dispatchEvent(new CustomEvent("input"));
      })
      .selectAll("path")
      .data(root.descendants())
      .join("path")
      .attr("d", this.mousearc())
      .on("mouseenter", (event, d) => {
        // Get the ancestors of the current segment, minus the root
        const sequence = d
          .ancestors()
          .reverse()
          .slice(1);
        // Highlight the ancestors
        path.attr("fill-opacity", node =>
          sequence.indexOf(node) >= 0 ? 1.0 : 0.2
        );
        const percentage = ((100 * d.value) / root.value).toPrecision(3);

        label.select(".nodeName")
          .text(d.data.name || '');

        label.select(".nodeSameAsName")
          .text(d.data.sameAsName || '');

        label.select(".nodeEnrichedSameAsName")
          .text(d.data.enrichedSameAsName || '');

        // Update the value of this view with the currently hovered sequence and percentage
        element.value = { sequence, percentage };
        element.dispatchEvent(new CustomEvent("input"));
      })
      .on("click", (event, d) => {
        if (this.props.onTextClick && d.data.type === 'target') {
          this.props.onTextClick(d.data);
        }
      });

    return element;
  };

  componentDidMount = () => this.renderChart();
  componentDidUpdate = () => this.renderChart();

  render = () => (
    <div ref={this.chart} />
  );
}
