import React, { useState, useCallback, useRef } from 'react';

import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';
import Modal from 'react-bootstrap/Modal';

const DisplayOptions = ({ relationTypes, relations, setRelations, onNewRelation, loading }) => {
  const fileInput = useRef(null);
  const [showUploadForm, setShowUploadForm] = useState(false);
  const [relation, setRelation] = useState("");
  const [newRelationName, setNewRelationName] = useState("");
  const [newRelation, setNewRelation] = useState("");

  const updateNewRelation = useCallback((name) => {
    setNewRelationName(name);
    const rname = name.split(' ').map(w => {
      if (!w) {
        return w;
      }

      return w[0].toUpperCase() + w.slice(1).toLowerCase()
    }).join('');
    setNewRelation(rname);
  }, []);

  const useOldRelation = (event) => {
    const r = event.target.value;
    setRelation(r);

    if (!r) {
      setNewRelationName("");
      setNewRelation("");
      return;
    }

    const rType = relationTypes.find(t => t.relation === r);
    setNewRelation(rType.relation);
    setNewRelationName(rType.name)
  };

  const openUploadForm = useCallback(() => {
    setShowUploadForm(true);
  }, [setShowUploadForm]);

  const closeUploadForm = useCallback(() => {
    setRelation("");
    setNewRelationName("");
    setNewRelation("");
    setShowUploadForm(false);
  }, [setShowUploadForm]);

  const saveNewRelation = () => {
    if (!fileInput.current) {
      return;
    }

    const data = new FormData();
    data.append('relation', newRelation);
    data.append('name', newRelationName);
    data.append('ttl', fileInput.current.files[0]);

    onNewRelation(data);

    closeUploadForm();
  };

  return (
    <Form inline onSubmit={(e) => e.preventDefault()} style={{marginTop: 20, marginBottom: 20}}>
      <Form.Group style={{ marginRight: 20 }}>
        <Form.Label style={{color: '#db1dcf', marginRight: 20 }}>Purple</Form.Label>
        <Form.Control
          as="select"
          value={relations[0]}
          onChange={(event) => setRelations([event.target.value, relations[1]])}
          disabled={loading}
        >
          {relationTypes.filter(type => type.relation !== relations[1]).map((type) =>
            <option key={type.relation} value={type.relation}>{type.name}</option>
          )}
        </Form.Control>
      </Form.Group>

      <Form.Group style={{ marginRight: 20 }}>
        <Form.Label style={{color: '#f48f0d', marginRight: 20 }}>Yellow</Form.Label>
        <Form.Control
          as="select"
          value={relations[1]}
          disabled={loading}
          onChange={(event) => setRelations([relations[0], event.target.value])}
        >
          {relationTypes.filter(type => type.relation !== relations[0]).map((type) =>
            <option key={type.relation} value={type.relation}>{type.name}</option>
          )}
        </Form.Control>
      </Form.Group>

      <Form.Group>
        <Button variant="primary" type="button" onClick={openUploadForm}>Add</Button>
      </Form.Group>

      <Modal show={showUploadForm} onHide={closeUploadForm} animation={false}>
        <Modal.Header closeButton>
          <Modal.Title>Add new algorithm</Modal.Title>
        </Modal.Header>

        <Modal.Body>
          <Form>
            <Form.Group>
              <Form.Label>Algorithm</Form.Label>
              <Form.Control
                as="select"
                value={relation}
                disabled={loading}
                onChange={useOldRelation}
              >
                {relationTypes.filter(type => !!type.relation).map((type) =>
                  <option key={type.relation} value={type.relation}>{type.name}</option>
                )}

                <option key="__new__" value="" style={{ fontWeight: 'bold' }}>Add new algorithm</option>
              </Form.Control>

              {(!relation) &&
                <Form.Control
                  placeholder="Enter unique algorithm name"
                  value={newRelationName}
                  onChange={(event) => updateNewRelation(event.target.value)}
                  style={{ marginTop: 20 }}
                />
              }
            </Form.Group>

            <Form.Group>
              <Form.Label>Select ttl file with links</Form.Label>
              <Form.Control type="file" ref={fileInput} />
            </Form.Group>
          </Form>
        </Modal.Body>

        <Modal.Footer>
          <Button variant="primary" onClick={saveNewRelation}>Save</Button>
          <Button variant="secondary" onClick={closeUploadForm}>Close</Button>
        </Modal.Footer>
      </Modal>
    </Form>
  );
};

export default DisplayOptions;
