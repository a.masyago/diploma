const Showcase = ({ data, goToId }) => {
  if (!data) {
    return (<div />);
  }

  const cn = (child) => !!child.sameAs ? 'clickable' : '';

  const onClick = (child, useSameAsId=false) => {
    if (child.sameAs) {
      if (useSameAsId) {
        goToId(child.sameAs.id);
      } else {
        goToId(child.id);
      }
    }
  }

  return (
    <div className="showcase">
      <div className="block parents-block">
        <h3 className="block-header">Parents</h3>

        {(data.matchedParents.map(child =>
          <div className="showcase-row" key={child.id}>
            <div className="showcase-col showcase-col-left">
              <div className={`concept child arrow-right arrow-sameAs ${cn(child)}`} onClick={() => onClick(child)}>
                {child.name}
              </div>
            </div>

            <div className="showcase-col showcase-col-right">
              <div className={`concept child sameAs ${cn(child)}`} onClick={() => onClick(child)}>
                {child.sameAs.name}
              </div>
            </div>
          </div>
        ))}

        {(data.unmatchedParents.map(child =>
          <div className="showcase-row" key={child.id}>
            <div className="showcase-col showcase-col-left">
              {(!!child.sameAs) &&
                <div className={`concept child sameAs ${cn(child)}`} onClick={() => onClick(child)}>
                  {child.sameAs.name}
                </div>
              }
              {(!!child.sameAs) &&
                <div className={`concept child arrow-left arrow-sameAs ${cn(child)}`} onClick={() => onClick(child)}>
                  {child.name}
                </div>
              }
              {(!child.sameAs) &&
                <div className={`concept child ${cn(child)}`} onClick={() => onClick(child)}>
                  {child.name}
                </div>
              }
            </div>

            <div className="showcase-col showcase-col-right" />
          </div>
        ))}

        {(data.sameAs.unmatchedParents.map(child =>
          <div className="showcase-row" key={child.id}>
            <div className="showcase-col showcase-col-left" />

            <div className="showcase-col showcase-col-right">
              <div className={`concept child sameAs ${cn(child)}`} onClick={() => onClick(child, true)}>
                {child.name}
              </div>

              {(!!child.sameAs) &&
                <div className={`concept child arrow-left arrow-sameAs ${cn(child)}`} onClick={() => onClick(child, true)}>
                  {child.sameAs.name}
                </div>
              }
            </div>
          </div>
        ))}
      </div>

      <div className="block target-block">
        <div className="showcase-row">
          <div className="showcase-col showcase-col-left">
            <div className="concept target arrow-right arrow-sameAs">
              {data.name}
            </div>
          </div>


          <div className="showcase-col showcase-col-right">
            <div className="concept target sameAs">
              {data.sameAs.name}
            </div>
          </div>
        </div>
      </div>

      <div className="block children-block">
        <h3 className="block-header">Children</h3>

        {(data.matchedChildren.map(child =>
          <div className="showcase-row" key={child.id}>
            <div className="showcase-col showcase-col-left">
              <div className={`concept child arrow-right arrow-sameAs ${cn(child)}`} onClick={() => onClick(child)}>
                {child.name}
              </div>
            </div>

            <div className="showcase-col showcase-col-right">
              <div className={`concept child sameAs ${cn(child)}`} onClick={() => onClick(child)}>
                {child.sameAs.name}
              </div>
            </div>
          </div>
        ))}

        {(data.unmatchedChildren.map(child =>
          <div className="showcase-row" key={child.id}>
            <div className="showcase-col showcase-col-left">
              {(!!child.sameAs) &&
                <div className={`concept child sameAs ${cn(child)}`} onClick={() => onClick(child)}>
                  {child.sameAs.name}
                </div>
              }


              {(!!child.sameAs) &&
                <div className={`concept child arrow-left arrow-sameAs ${cn(child)}`} onClick={() => onClick(child)}>
                  {child.name}
                </div>
              }
              {(!child.sameAs) &&
                <div className={`concept child ${cn(child)}`} onClick={() => onClick(child)}>
                  {child.name}
                </div>
              }
            </div>

            <div className="showcase-col showcase-col-right" />
          </div>
        ))}

        {(data.sameAs.unmatchedChildren.map(child =>
          <div className="showcase-row" key={child.id}>
            <div className="showcase-col showcase-col-left" />

            <div className="showcase-col showcase-col-right">
              <div className={`concept child sameAs ${cn(child)}`} onClick={() => onClick(child, true)}>
                {child.name}
              </div>

              {(!!child.sameAs) &&
                <div className={`concept child arrow-left arrow-sameAs ${cn(child)}`} onClick={() => onClick(child, true)}>
                  {child.sameAs.name}
                </div>
              }
            </div>
          </div>
        ))}
      </div>
    </div>
  );
}

export default Showcase;
