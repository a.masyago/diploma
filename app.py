from time import sleep

from aiohttp import web

from diploma import handlers as h
from diploma import settings
from diploma.db import init as init_db, prefill
from diploma.rdf_controller import RdfController


async def close_controller(app):
    app.rdf.close()


def create_app():
    init_db()
    prefill()

    app = web.Application(client_max_size=settings.CLIENT_MAX_SIZE)
    try:
        app.rdf = RdfController(settings.NEO4J_ADDRESS, settings.NEO4J_USER, settings.NEO4J_PASSWORD)
    except Exception:
        print('Neo4j is not ready yet')
        sleep(3)
        return None

    app.router.add_get('/api/healthcheck/', h.healthcheck)
    app.router.add_get('/api/search/', h.search)
    app.router.add_get('/api/relations/', h.get_relations)
    app.router.add_post('/api/new-relation/', h.post_relations)

    app.router.add_get('/api/tidy_tree/', h.get_tidy_tree)

    app.on_cleanup.append(close_controller)

    return app


if __name__ == '__main__':
    main_app = create_app()

    if main_app:
        print(f'Running API on http://{settings.HOST}:{settings.PORT}/')
        web.run_app(main_app, host=settings.HOST, port=settings.PORT)
