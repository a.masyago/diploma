from diploma.rdf_controller import RdfController
import re
from algorithm.utils.finder import entity_generator, entity_generator_no_split


def find_entity_by_irl_at_file(entities_irl, file_with_entities):
    with open(file_with_entities, "r") as fwe:
        for tmp_entity in entity_generator_no_split(fwe):
            if entities_irl == tmp_entity.irl:
                return True

        return False


def get_categories_from_db_using_file(file_with_categories):
    with open(file_with_categories, "r") as file_with_categories:
        categories = list()

        for tmp_entity in entity_generator_no_split(file_with_categories): 
            category_irl = tmp_entity.irl
            categories.append(category_irl)
        
        list_of_categories = categories
        r = rdf.get_all_children(list_of_categories)

        r = delete_repetition_in_results(r)

        r = delete_strange_categories_in_results(r)

        return r


def delete_repetition_in_results(r):
    count = 0

    unique_irl = set ()
    unique_results = []

    for result in r:
        # print(result)
        if result[0] not in unique_irl:
            
            unique_irl.add(result[0])
            #NEW ENTITY 
            unique_results.append((result[0], result[1]))
        else:
            count += 1

    print("deleted repetition: ", count)
    return unique_results


def delete_strange_categories_in_results(r):
    counter = 0 
    reg_wiki = []
    reg_spiski = []
    reg_personalii = []
    reg_portal = []

    filtered_r = []

    for record in r:    
        reg_wiki = re.findall(r'Википедия', record[0])
        reg_spiski = re.findall(r'Списки', record[0])
        reg_personalii = re.findall(r'Персоналии', record[0])
        reg_portal = re.findall(r'Портал', record[0])

        if reg_wiki == [] and reg_spiski == [] and reg_personalii == [] and reg_portal == [] :
            filtered_r.append(record)
        else:
            counter += 1

    # print("filtered_r:", filtered_r)
    # print("Killed", counter, "entities")

    return filtered_r


def add_unique_irl_in_file(results, file_with_categories):
    unique_irl = set ()

    with open(file_with_categories, "r") as file_wc:

        irl = file_wc.readline().rstrip()
        name = file_wc.readline().rstrip()

        while irl:
            unique_irl.add(irl)
            irl = file_wc.readline().rstrip()
            name = file_wc.readline().rstrip()
        
    with open(file_with_categories, "a+") as file_wc:
        deleted = 0
        added = 0

        for result in results:
            if result[0] not in unique_irl:

                file_wc.write(result[0])
                file_wc.write('\n')
                file_wc.write(result[1])
                file_wc.write('\n')
                added += 1
            
            else:
                deleted += 1
        
        print("added: ", added)
        print("deleted: ", deleted)


if __name__ == '__main__':
    rdf = RdfController("bolt://localhost:7687", "neo4j", "test")

    print("This was a triumph")
    print("I'm making a note here")
    print("Huge success")

    ru_file = "algorithm/utils/children_ru.txt"
    en_file = "algorithm/utils/children_en.txt"

    # r = get_categories_from_db_using_file("algorithm/utils/nauka.txt")

    # # # RU RU RUSSIA
    # r = get_categories_from_db_using_file(ru_file)
    # add_unique_irl_in_file(r, ru_file)

    # # # EN EN ENGLAND
    # r = get_categories_from_db_using_file(en_file)
    # add_unique_irl_in_file(r, en_file)

    
    target_irl = "http://wikidata.dbpedia.org/resource/Q1458083"
    R = rdf.get_same_as_and_children_of_same_as(target_irl)

    print(R)


    # Count repetitions @ file
    #
    # with open(en_file) as f:
    #     seen = set()
    #     repetitions = 0
    #     for line in f:
    #         line_lower = line.lower()
    #         if line_lower in seen:
    #             repetitions += 1
    #         else:
    #             seen.add(line_lower)

    # print("repetitions: ", repetitions)

     ################################### JUST first one
    
    # with open("algorithm/utils/tmp_results.txt", "w+") as output_file:
    #     for record in r:
    #         # print(record[0])
    #         # print(record[1])

    #         reg_wiki = re.findall(r'Википедия', record[0])
    #         reg_spiski = re.findall(r'Списки', record[0])
    #         reg_personalii = re.findall(r'Персоналии', record[0])
    #         reg_portal = re.findall(r'Портал', record[0])

    #         if reg_wiki == [] and reg_spiski == [] and reg_personalii == [] and reg_portal == [] :
    #             output_file.write(record[0])
    #             output_file.write("\n")
    #             output_file.write(record[1])
    #             output_file.write("\n")
    #             output_file.flush()
    #         else:
    #             counter += 1

    # print("Killed", counter, "entities")

    ################################### DO IT 10 times

    # with open("algorithm/utils/tmp_results.txt", "r") as nauka_file:
    #     categories = list()

    #     for tmp_entity in entity_generator_no_split(nauka_file): 
    #         category_irl = tmp_entity.irl
    #         categories.append(category_irl)

    #     print("NAMEZZZZZZZ", categories)
        
    #     list_of_categories = categories
    #     r = rdf.get_all_children(list_of_categories)
    #     print("R:")
    #     print(r)

    # with open("algorithm/utils/results.txt", "a+") as output_file:
    #     for record in r:
    #         # print(record[0])
    #         # print(record[1])

    #         reg_wiki = re.findall(r'Википедия', record[0])
    #         reg_spiski = re.findall(r'Списки', record[0])
    #         reg_personalii = re.findall(r'Персоналии', record[0])
    #         reg_portal = re.findall(r'Портал', record[0])

    #         if reg_wiki == [] and reg_spiski == [] and reg_personalii == [] and reg_portal == [] :
    #             output_file.write(record[0])
    #             output_file.write("\n")
    #             output_file.write(record[1])
    #             output_file.write("\n")
    #             output_file.flush()
    #         else:
    #             counter += 1

    #     print("Killed", counter, "entities")


    # with open("algorithm/utils/tmp_results.txt", "w+") as output_file:
    #     for record in r:
    #         # print(record[0])
    #         # print(record[1])

    #         reg_wiki = re.findall(r'Википедия', record[0])
    #         reg_spiski = re.findall(r'Списки', record[0])
    #         reg_personalii = re.findall(r'Персоналии', record[0])
    #         reg_portal = re.findall(r'Портал', record[0])

    #         if reg_wiki == [] and reg_spiski == [] and reg_personalii == [] and reg_portal == [] :
    #             output_file.write(record[0])
    #             output_file.write("\n")
    #             output_file.write(record[1])
    #             output_file.write("\n")
    #             output_file.flush()
    #         else:
    #             counter += 1

    #     print("Killed", counter, "entities")


    ############## Filtering

    # with open("algorithm/utils/results.txt", "r") as results:
    #     with open("algorithm/utils/filtered_results.txt", "w") as of:
    #         count = 0

    #         for tmp_entity in entity_generator_no_split(results): 
    #             count += 1

    #             if find_entity_at_file(tmp_entity, "algorithm/utils/results.txt") == False:
    #                 of.write(tmp_entity.irl)
    #                 of.write("\n")
    #                 of.write(tmp_entity.name)
    #                 of.write("\n")

    #             print("tmpcount = ", count)

    # print("count = ", count)



    # OLD ONE METHOD FOR entities_en
    # r = rdf.gimme_more(0)
    # step = 1000
    # skipping_lines = 0

    # with open("algorithm/utils/entities_en.txt", "w+") as output_file:
    #     while True:
    #         if not r:
    #             break 
            
    #         for record in r:
    # #            print(record[0])
    # #            print(record[1])
    #             output_file.write(record[0])
    #             output_file.write("\n")
    #             output_file.write(record[1])
    #             output_file.write("\n")
    #             output_file.flush()

    #         skipping_lines = skipping_lines + step
    #         print("Printed for now: ", skipping_lines)
            
    #         r = rdf.gimme_more(skipping_lines)
                        
 #   print(r)
