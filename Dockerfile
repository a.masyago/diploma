FROM python:3.8

RUN mkdir /app
WORKDIR /app

COPY requirements.txt ./
RUN pip install -r requirements.txt

COPY diploma ./diploma
COPY app.py ./

EXPOSE 7070
CMD ["python", "app.py"]
