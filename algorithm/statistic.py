import json
from datetime import datetime
from multiprocessing import Lock
from multiprocessing.pool import Pool

from nltk.corpus import wordnet as wn

from utils.finder import Entity, entity_generator


def get_stat():
    ru_file = open("utils/entities_en_preprocessed.txt", "r")
    length = [0]*1024*1024

    count = 0
    offset = 0
    limit = 1000
    for tmp_entity in get_name_length(ru_file):
        count += 1

        if count <= offset:
            continue

        tmp_length = len(tmp_entity.name)
        # print(tmp_length)
        length[tmp_length] += 1

        # if count > offset + limit:
        #     break

    for i in range (0, 128):
        if length[i] != 0:
            print(i, ": ", length[i])

    total = sum(length)
    avg = sum([x * i for i, x in enumerate(length)]) / total
    print(f'total: {total}, avg: {avg}')


def compare_words(synset1, synset2):
    bestscore = 0
    for x in synset1:
        for y in synset2:
            try:
                score = x.path_similarity(y)
            except Exception:
                score = 0

            if score > bestscore:
                bestscore = score

            if bestscore == 1.0:
                return bestscore

    return bestscore


lock = None
synsets = None

def init(l, s):
    global lock
    lock = l

    global synsets
    synsets = s


def worker(args):
    global synsets
    word_ru, word_en = args
    key = f'{min(word_ru, word_en)}||{max(word_ru, word_en)}'

    if word_ru == word_en:
        score = 100
    else:
        score = compare_words(synsets[word_ru], synsets[word_en])
        score = int(score * 100)

    if score > 20:
        lock.acquire()

        # with open('utils/scores.txt', 'a+') as f:
        with open('utils/yandex_scores.txt', 'a+') as f:
            f.write(f'{key} {score}\n')

        lock.release()


def get_word_stat():
    # ru_file = open("utils/entities_ru_preprocessed.txt", "r")
    ru_file = open("utils/entities_ru_yandex_preprocessed.txt", "r")
    words_ru = set()
    syns = dict()
    
    # en_file = open("utils/entities_en_preprocessed.txt", "r")
    en_file = open("utils/entities_en_preprocessed.txt", "r")
    words_en = set()

    for tmp_entity in entity_generator(ru_file):
        for word in tmp_entity.name:
            words_ru.add(word)

            if word not in syns:
                syns[word] = wn.synsets(word)[:3]

    for tmp_entity in entity_generator(en_file):
        for word in tmp_entity.name:
            words_en.add(word)

            if word not in syns:
                syns[word] = wn.synsets(word)[:3]

    scores = dict()
    count = 0
    total = len(words_ru)
    print(f'unique ru: {len(words_ru)}, {len(syns)}')
    print(f'unique en: {len(words_en)}, {len(syns)}')

    l = Lock()
    pool_size = 8
    pool = Pool(processes=pool_size, initializer=init, initargs=(l, syns))

    start = datetime.now()
    tasks = set()
    for word_ru in words_ru:
        count += 1
        for word_en in words_en:
            tasks.add((word_ru, word_en))

        pool.map(worker, tasks)
        tasks = set()

        if count % 100 == 0:
            now = datetime.now()
            diff = (now - start).total_seconds()
            start = now
            print(f'{diff}:\t{count}, {count / total}')
            
    
if __name__ == '__main__':
    get_word_stat()