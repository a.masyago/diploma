from string import digits
from datetime import datetime
from multiprocessing import Lock
from multiprocessing.pool import Pool

from utils.wordnet import compare_sentences_using_scores


class Entity(object):
    def __init__(self, irl, name):
        self.irl = irl
        self.name = name

    def __str__(self):
        return f"irl: {self.irl}, name: {self.name}"


def get_entity(file):
    irl = file.readline().rstrip()
    name = file.readline().rstrip()

    if irl:
        entity = Entity(irl, name)
        return entity


def entity_generator(entity_file):
    irl = entity_file.readline().rstrip()
    name = entity_file.readline().rstrip().split()

    while irl:
        if name and name[0][0] not in digits:
            yield Entity(irl, name)

        irl = entity_file.readline().rstrip()
        name = entity_file.readline().rstrip().split()


def entity_generator_no_split(entity_file):
    irl = entity_file.readline().rstrip()
    name = entity_file.readline().rstrip()

    while irl:
        if name and name[0][0] not in digits:
            yield Entity(irl, name)

        irl = entity_file.readline().rstrip()
        name = entity_file.readline().rstrip()


lock = None
en_entities = []
scores = {}


def find_best_for_one_preprocessed(tmp_entity, threshold):
    global en_entities
    global scores

    # print("Started @ find_best_for_one", tmp_entity)

    best_score = 0
    best_irl = ""
    best_name = ""

    for tmp_entity_en in en_entities:
        tmp_score = compare_sentences_using_scores(tmp_entity.name, tmp_entity_en.name, scores=scores)

        if tmp_score > best_score:
            best_score = tmp_score
            best_irl = tmp_entity_en.irl
            best_name = tmp_entity_en.name

    if best_score > threshold:
        # print(f"Got match: {best_score}\t{tmp_entity.name} || {best_name}")

        lock.acquire()

        try:
            # for Argos translate
            #
            # with open("utils/generated_output_with_scores.txt", "a+") as output_file:
            with open("utils/generated_output_with_yandex_scores.txt", "a+") as output_file:
                output_file.write(str(best_score))
                output_file.write('\n')

                output_file.write(tmp_entity.irl)
                output_file.write(" ")
                output_file.write(best_irl)
                output_file.write(" ")
                output_file.write(" ".join(best_name))
                output_file.write("\n")
        except Exception as e:
            print(e)

        lock.release()
        # print("[[[==================== NEXT =====================]]]")


def find_task(args):
    return find_best_for_one_preprocessed(*args)


def init(lock_var, en_entities_var, scores_var):
    global lock
    lock = lock_var

    global en_entities
    en_entities = en_entities_var

    global scores
    scores = scores_var


def find_best_for_everyone_preprocessed(threshold):
    # For argos translate
    #
    # ru_file = open("utils/entities_ru_preprocessed.txt", "r")

    ru_file = open("utils/entities_ru_yandex_preprocessed.txt", "r")

    lock_var = Lock()
    en_entities = read_preprocessed_entities()
    print(f'num en_entities {len(en_entities)}')
    scores = read_scores()

    pool_size = 8

    pool = Pool(processes=pool_size, initializer=init, initargs=(lock_var, en_entities, scores))

    print(f'=================== start {datetime.now().isoformat()} ================')
    tasks = set()
    for tmp_entity in entity_generator(ru_file):
        tasks.add((tmp_entity, threshold))

        if len(tasks) == pool_size * 100:
            pool.map(find_task, tasks)
            tasks = set()
            print(f'=================== pack {datetime.now().isoformat()} ================')

    pool.map(find_task, tasks)

    pool.close()
    pool.join()

    ru_file.close()


def read_preprocessed_entities():
    with open("utils/entities_en_preprocessed.txt", "r") as en_file:
        return [entity for entity in entity_generator(en_file)]


def read_scores():
    scores = {}
    # for Argos translate
    #
    # with open('utils/scores.txt', 'r') as scores_file:
    with open('utils/yandex_scores.txt', 'r') as scores_file:
        for line in scores_file:
            key, score = line.split()

            scores[key] = int(score)

    return scores
