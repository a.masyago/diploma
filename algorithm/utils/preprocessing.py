import nltk, re

from nltk.corpus import stopwords
from nltk.tokenize import word_tokenize 
from nltk.tokenize import WordPunctTokenizer
from re import finditer
import numpy as np
import itertools


stop_words = set(stopwords.words('english'))

def preprocess(sentence):
    return deleteStopWords(toLowerCase(splitCamelCase(deletePunkt(WordPunctTokenize(sentence)))))

def WordPunctTokenize(sentence):
    tokenizer = WordPunctTokenizer()

    words = tokenizer.tokenize(sentence)
    return words


punkt_re = re.compile('[0-9A-Za-z]+')


def deletePunkt(words):
    words_without_punkt = [x for x in words if punkt_re.match(x)]

    return words_without_punkt


camel_case_re = re.compile('.+?(?:(?<=[a-z])(?=[A-Z])|(?<=[A-Z])(?=[A-Z][a-z])|$)')


def camel_case_split(identifier):
    matches = camel_case_re.finditer(identifier)

    return [m.group(0) for m in matches]


def splitCamelCase(words):
    not_flat_list = [camel_case_split(x) for x in words]

    flat_list = list(itertools.chain(*not_flat_list))
    
    return flat_list


def toLowerCase(words):
    return [x.lower() if len(x) == 1 or x.upper() != x else x for x in words]

def deleteStopWords(words):
    filtered_sentence = [w for w in words if not w in stop_words]

    return filtered_sentence



def filter_sentence2(sentence):
    tokenizer = WordPunctTokenizer()    

    sentence = tokenizer.tokenize(sentence)

    return sentence

def filter_sentence(sentence):
    # print("Sentence before filtering: ", sentence)
    
    # Удаляю пробелы
    splitted_sentence = sentence.split()
    sentence = list(filter(bool, splitted_sentence))
    # print("Sentence without white spaces: ", sentence)

    # Удаляем стоп слова
    word_tokens = word_tokenize(sentence)
    # print("stopwords: ", stop_words)
    # print("word tokens: ", word_tokens)

    filtered_sentence = [w for w in word_tokens if not w in stop_words]
 #   print("Tokenized + filtered sentence: ", sentence)

    return word_tokens




