import json
from time import sleep
import requests

URL = "https://translate.api.cloud.yandex.net/translate/v2/translate" 
KEY = "t1.9euelZqXks2WlsyNzImWnJiYzsidnu3rnpWalpOWx5aWnI2Tlo-OmI7KnMjl8_cnNgV8-e83VE9a_N3z92dkAnz57zdUT1r8.WVEJbzHI3k-Rv6ja-yecqhl6wgSWMhn6SFZNw34y-8duJtgm1r6kt3JdY4wMtAwtRuFu7tI7ACH-iZ6BhqX4CQ" 

headers = {
    'Content-Type': "application/json",
    'Authorization': f"Bearer {KEY}",
}


def translate_me(texts):
    sleep(0.1)

    data = {
        "folder_id": "b1gbmh8bt04b6s1sq883",
        "texts": texts,
        "sourceLanguageCode": "ru",
        "targetLanguageCode": "en",
    }
    response = requests.post(URL, json = data, headers = headers)

    if response.status_code != 200:
        print(texts)
        raise Exception(response.text)

    return response.json().get('translations', [])


if __name__ == '__main__':
    json = translate_me(["Привет мой друг"])
    print(json)
