from argostranslate import package, translate


package.install_from_path('/opt/diploma/argos/ru_en.argosmodel')
installed_languages = translate.get_installed_languages() 

translator = None

def init():
    global translator
    translator = installed_languages[1].get_translation(installed_languages[0])


def translate(word):
    return translator.translate(word)


def translate_sentence(sentence):
    # print("translated: ",translator.translate(sentence))
    return translator.translate(sentence)
