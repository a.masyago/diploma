
def postprocess_entity(input_file_name, output_file_name, threshold):
    input_file = open(input_file_name, 'r')
    output_file = open(output_file_name, 'w+')

    line = input_file.readline()

    while line:
        if float(line) >= threshold:
            # print("WORKS!", line, threshold)
            entities_line = input_file.readline()

            if entities_line != "":
                ru_irl, en_irl, *_ = entities_line.split()

                output_file.write("<")
                output_file.write(ru_irl)
                output_file.write("> ")

                # FOR enrichedSameAs
                # output_file.write("<http://www.w3.org/2002/07/owl#enrichedSameAs> ")
                output_file.write("<http://www.w3.org/2002/07/owl#yandexSameAs> ")

                output_file.write("<")
                output_file.write(en_irl)
                output_file.write("> .")

                output_file.write("\n")

        line = input_file.readline()
