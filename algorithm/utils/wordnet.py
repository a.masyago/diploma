from nltk.corpus import wordnet as wn
# from utils.translation import translate_sentence
from utils.preprocessing import preprocess

def get_jaccard_similarity(list1, list2):
    s1 = set(list1)
    s2 = set(list2)
    return float(len(s1.intersection(s2)) / len(s1.union(s2)))


def compare_words(word1, word2):
    synset1 = wn.synsets(word1)
    synset2 = wn.synsets(word2)

    bestscore = 0
    for x in synset1[:3]: 
        for y in synset2[:3]:
            score = x.path_similarity(y)
            if score > bestscore:
                bestscore = score

            if bestscore == 1.0:
                return bestscore
    
    return bestscore


def compare_ru_eng_sentences(sentence1, sentence2):
    sentence1 = translate_sentence(sentence1)

    sentence1 = preprocess(sentence1)
    sentence2 = preprocess(sentence2)

    return compare_sentences(sentence1, sentence2)


def compare_sentences(sentence1, sentence2):
    similarity = get_jaccard_similarity(sentence1, sentence2)
    # print("JACCARD == ", similarity)
    if similarity != 1:
        similarity = sentence_similarity(sentence1, sentence2)

    return similarity


def sentence_similarity(sentence1, sentence2):
    sim_score = 0

    if not sentence1 or not sentence2:
        return

    sim_score_reverse = [0] * len(sentence2)
#    for word_at_s1 in sentence1.split():
    for word_at_s1 in sentence1:
        score = 0

#        for word_at_s2 in sentence2.split():
        for i, word_at_s2 in enumerate(sentence2):
            words_similarity = compare_words(word_at_s1, word_at_s2)
            if words_similarity > score:
                score = words_similarity
            
            if words_similarity > sim_score_reverse[i]:
                sim_score_reverse[i] = words_similarity
        
        sim_score += score

    sim_score = sim_score / len(sentence1)
    sim_score_reverse = sum(sim_score_reverse) / len(sentence2) 

    # print ("sim_score: ", sim_score)
    # print ("Count: ", count)
    # print ("sentence similarity: ", sim_score/count)

    return (sim_score + sim_score_reverse) / 2


def compare_sentences_using_scores(sentence1, sentence2, scores=None):
    if scores is None:
        scores = {}

    similarity = get_jaccard_similarity(sentence1, sentence2)
    if similarity == 1:
        return 1
    
    if similarity != 1:
        if len(sentence1) > 1 and len(sentence2) > 1:
            similarity = sentence_similarity_using_scores(sentence1, sentence2, scores=scores)
        else:
            similarity = 0

    return similarity


def sentence_similarity_using_scores(sentence1, sentence2, scores=None):
    sim_score = 0

    if not sentence1 or not sentence2:
        return

    sim_score_reverse = [0] * len(sentence2)
    for word_at_s1 in sentence1:
        score = 0

        for i, word_at_s2 in enumerate(sentence2):
            words_similarity = compare_words_using_scores(word_at_s1, word_at_s2, scores=scores)
            if words_similarity > score:
                score = words_similarity
            
            if words_similarity > sim_score_reverse[i]:
                sim_score_reverse[i] = words_similarity
        
        sim_score += score

    sim_score = sim_score / len(sentence1)
    sim_score_reverse = sum(sim_score_reverse) / len(sentence2) 

    return (sim_score + sim_score_reverse) / 2


def compare_words_using_scores(word1, word2, scores=None):
    if word1 == word2:
        return 1.0

    key = f'{min(word1, word2)}||{max(word1, word2)}'
    score = scores.get(key, 0)
    
    return score / 100.0
