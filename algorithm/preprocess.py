from utils.finder import entity_generator, entity_generator_no_split
from utils.preprocessing import preprocess
# from utils.translation import translate_sentence, init as init_translator


if __name__ == '__main__':
    # init_translator()

    # ru_file = "utils/children_ru.txt"
    ru_file = "utils/entities_ru_translated_yandex.txt"
    
    # en_file = "utils/children_en.txt"

    # with open(ru_file) as f, open('utils/entities_ru_preprocessed.txt', 'w+') as o:
    with open(ru_file) as f, open('utils/entities_ru_yandex_preprocessed.txt', 'w+') as o:
        # ADDED no_split 
        for entity in entity_generator_no_split(f):
            try:
                translated = entity.name
                p = preprocess(translated)
            except Exception as e:
                print(entity, e)
                continue
            
            o.write(entity.irl)
            o.write('\n')
            o.write(' '.join(p))
            o.write('\n')

    # with open(en_file) as f, open('utils/entities_en_preprocessed.txt', 'w+') as o:
    #     # ADDED no_split
    #     for entity in entity_generator_no_split(f):
    #         try:
    #             p = preprocess(entity.name)
    #         except Exception as e:
    #             print(entity, e)
    #             continue
            
    #         o.write(entity.irl)
    #         o.write('\n')
    #         o.write(' '.join(p))
    #         o.write('\n')