from utils.finder import entity_generator_no_split
from utils.yandex_translation import translate_me

from utils.finder import entity_generator, entity_generator_no_split
from utils.preprocessing import preprocess
from utils.translation import translate_sentence, init as init_translator


MAX_SYMBOLS_LIMIT = 9000

def translate_file ():
    ru_file = "utils/children_ru.txt"

    with open(ru_file) as f, open('utils/entities_ru_translated_yandex.txt', 'w+') as o:
        entities = list()
        length = 0

        for entity in entity_generator_no_split(f):
            if (length + len(entity.name)) < MAX_SYMBOLS_LIMIT:
                entities.append(entity)
                length += len(entity.name)
                continue
            
            texts = [x.name for x in entities]

            results = translate_me(texts)

            for ent, result in zip(entities, results):
                o.write(ent.irl)
                o.write('\n')
                o.write(result.get("text", ""))
                o.write('\n')

            entities = [entity]
            length = len(entity.name)


if __name__ == '__main__':
    # DONT TOUCH. ITS DONE
    #
    # translate_file()

    # ALREADY DONE
    #     
    # ru_file = "utils/entities_ru_translated_yandex.txt"

    # with open(ru_file) as f, open('utils/entities_ru_yandex_preprocessed.txt', 'w+') as o:
    #     # ADDED no_split 
    #     for entity in entity_generator_no_split(f):
    #         try:
    #             translated = entity.name
    #             p = preprocess(translated)
    #         except Exception as e:
    #             print(entity, e)
    #             continue
            
    #         o.write(entity.irl)
    #         o.write('\n')
    #         o.write(' '.join(p))
    #         o.write('\n')

    

            