from utils.postprocessing import postprocess_entity


if __name__ == '__main__':
    # in_file_name = "utils/generated_output_with_scores.txt"
    in_file_name = "utils/generated_output_with_yandex_scores.txt"
    out_file_name = "utils/generated_links_yandex.ttl"

    threshold = 0.5

    print("Start postprocess")
    postprocess_entity(in_file_name, out_file_name, threshold)

    print("End postprocess")
