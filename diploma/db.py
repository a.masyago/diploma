import os
import sqlite3
from diploma import settings


def get_db_path():
    if not settings.SQLITE_FOLDER:
        return 'relations.db'

    return os.path.join(settings.SQLITE_FOLDER, 'relations.db')


def init():
    con = sqlite3.connect(get_db_path())
    cur = con.cursor()
    cur.execute('''CREATE TABLE IF NOT EXISTS relations (name text, relation text)''')
    con.commit()


def prefill():
    relations = [
        {'name': 'Interlanguage Links', 'relation': 'sameAs'},
        {'name': 'OECM [Argos]', 'relation': 'enrichedSameAs'},
        {'name': 'OECM [Yandex]', 'relation': 'yandexSameAs'}
    ]

    con = sqlite3.connect(get_db_path())
    cur = con.cursor()
    cur.execute('''DELETE FROM relations''')
    for r in relations:
        cur.execute('''INSERT INTO relations VALUES (:name, :relation)''', r)
    con.commit()


def add_relation(name, relation):
    con = sqlite3.connect(get_db_path())
    cur = con.cursor()
    cur.execute('''INSERT INTO relations VALUES (:name, :relation)''', {'name': name, 'relation': relation})
    con.commit()


def load_relations():
    con = sqlite3.connect(get_db_path())
    cur = con.cursor()
    relations = []
    for row in cur.execute('SELECT name, relation FROM relations'):
        relations.append({'name': row[0], 'relation': row[1]})

    return relations
