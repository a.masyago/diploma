import re

from cachetools import cached, LRUCache
from cachetools.keys import hashkey


def delete_strange_categories_in_results(r):
    counter = 0
    reg_wiki = []
    reg_spiski = []
    reg_personalii = []
    reg_portal = []

    filtered_r = []

    for record in r:
        reg_wiki = re.findall(r'Википедия', record[1]['uri'])
        reg_spiski = re.findall(r'Списки', record[1]['uri'])
        reg_personalii = re.findall(r'Персоналии', record[1]['uri'])
        reg_portal = re.findall(r'Портал', record[1]['uri'])

        rw_end = re.findall(r'Википедия', record[3]['uri'])
        rs_end = re.findall(r'Списки', record[3]['uri'])
        rper_end = re.findall(r'Персоналии', record[3]['uri'])
        rpor_end = re.findall(r'Портал', record[3]['uri'])

        if reg_wiki == [] and reg_spiski == [] and reg_personalii == [] and reg_portal == [] and rw_end == [] and rs_end == [] and rper_end == [] and rpor_end == []:
            filtered_r.append(record)
        else:
            counter += 1

    # print("filtered_r:", filtered_r)
    print("Killed", counter, "entities")

    return filtered_r


def print_tree(tree, level=0):
    if level == 0:
        tree = [tree]

    for item in tree:
        item_label = item.get('node', {}).get('skos__prefLabel', '')
        item_children_count = len(item.get("children", []))
        item_same_as = item.get('same_as', {}).get('skos__prefLabel', '')
        print('\t' * level + f'{item_label} ({item_children_count}) || {item_same_as}')

        if 'children' in item:
            print_tree(item['children'], level=level+1)


def collect_node_uris(tree):
    uris = [tree['node']['uri']]

    for child in tree.get('children', []):
        uris += collect_node_uris(child)

    return uris


def collect_same_as_uris(same_as_with_children):
    return [source['uri'] for (target, source, child) in same_as_with_children] + \
        [target['uri'] for (target, source, child) in same_as_with_children] + \
        [child['uri'] for (target, source, child) in same_as_with_children if child]


def link_same_as(tree_node, result, label='same_as'):
    same_as_children = set()

    for same_as, same_as_child in result:
        if label not in tree_node:
            tree_node[label] = same_as

        if same_as.id != tree_node[label].id:
            continue

        if same_as_child:
            if not hasattr(same_as_child, 'parent_ids'):
                same_as_child.parent_ids = set()

            same_as_child.parent_ids.add(str(same_as.id))

            if tree_node.get('children'):
                same_as_children.add(same_as_child)

    return same_as_children


def add_same_as(same_as_with_children, tree_node, label='same_as'):
    if not tree_node.get('type'):
        tree_node['type'] = 'target'

    if not tree_node['type'] == 'target':
        return

    result = [(source, child) for (target, source, child) in same_as_with_children if target['uri'] == tree_node['node']['uri']]

    same_as_children = link_same_as(tree_node, result, label=label)

    children_same_as_ids = set()

    for child in tree_node['children']:
        add_same_as(same_as_with_children, child, label=label)

        if child.get(label):
            children_same_as_ids.add(child[label].id)

    for same_as_child in same_as_children:
        if same_as_child.id in children_same_as_ids:
            continue

        tree_node['children'].append({
            'type': label,
            label: same_as_child
        })


def add_subjects(target_subjects, tree_node):
    if 'type' not in tree_node or tree_node['type'] != 'target':
        return

    tree_node['subjects'] = {'node': tree_node['node'], 'type': 'target'}
    tree_node['subjects']['children'] = [{'type': 'target', 'node': subject} for (target_uri, subject) in target_subjects if target_uri == tree_node['node']['uri']]

    for child in tree_node.get('children', []):
        add_subjects(target_subjects, child)


def add_same_as_subjects(subjects_with_same_as, subjects_without_same_as, tree_node, label='same_as'):
    if 'type' not in tree_node or tree_node['type'] != 'target':
        return

    target_subject_uris = {}
    for child in tree_node['subjects']['children']:
        if 'node' in child:
            target_subject_uris[child['node']['uri']] = child

    if tree_node.get(label):
        tree_node['subjects'][label] = tree_node[label]

        same_as_subjects = [(subject, target_uri) for (same_as_uri, subject, target_uri) in subjects_with_same_as if same_as_uri == tree_node[label]['uri']]
        same_as_subjects_without_same_as = [subject for (same_as_uri, subject) in subjects_without_same_as if same_as_uri == tree_node[label]['uri']]

        for same_as_subject, target_uri in same_as_subjects:
            if target_uri in target_subject_uris:
                target_subject_uris[target_uri][label] = same_as_subject
                if not hasattr(same_as_subject, 'parent_ids'):
                    same_as_subject.parent_ids = set()
                same_as_subject.parent_ids.add(str(tree_node[label].id))
            else:
                same_as_subjects_without_same_as.append(same_as_subject)

        for same_as_subject in same_as_subjects_without_same_as:
            if not hasattr(same_as_subject, 'parent_ids'):
                same_as_subject.parent_ids = set()
            same_as_subject.parent_ids.add(str(tree_node[label].id))

        tree_node['subjects']['children'] += [{'type': label, label: x} for x in same_as_subjects_without_same_as]

    for child in tree_node.get('children', []):
        add_same_as_subjects(subjects_with_same_as, subjects_without_same_as, child, label=label)


@cached(cache=LRUCache(maxsize=32), key=lambda _, target_uri, relations: hashkey(target_uri, ",".join(relations)))
def build_tree(rdf, target_uri, relations):
    tree = rdf.get_children_path_by_uri(target_uri)

    if not tree['node']:
        tree['node'] = rdf.get_by_uri(target_uri)
        tree['children'] = []

    tree_uris = collect_node_uris(tree)
    uri_collections = {}

    for label, relation in zip(['same_as', 'enriched_same_as'], relations):
        if not relation:
            uri_collections[label] = []
            continue

        same_as_with_children = rdf.get_same_as_and_children_of_same_as_many(tree_uris, relation)
        uri_collections[label] = collect_same_as_uris(same_as_with_children)
        add_same_as(same_as_with_children, tree, label=label)

    target_subjects = rdf.get_subjects_many(tree_uris)
    add_subjects(target_subjects, tree)

    for label, relation in zip(['same_as', 'enriched_same_as'], relations):
        if not relation:
            continue

        subjects_with_same_as, subjects_without_same_as = rdf.get_subjects_many_with_same_as(uri_collections[label], link_name=relation)
        add_same_as_subjects(subjects_with_same_as, subjects_without_same_as, tree, label=label)

    return tree


class Dummy():
    @property
    def id(self):
        return None


def parent_ids(node):
    if not node:
        return None

    if not hasattr(node, 'parent_ids'):
        return None

    return list(node.parent_ids)


def tree_to_json(tree):
    dummy = Dummy()
    json = {
        'id': tree.get('node', dummy).id,
        'name': tree.get('node', {}).get('skos__prefLabel') or tree.get('node', {}).get('rdfs__label'),
        'uri': tree.get('node', {}).get('uri'),
        'type': tree.get('type'),
        'sameAsName': tree.get('same_as', {}).get('skos__prefLabel') or tree.get('same_as', {}).get('rdfs__label') or tree.get('same_as', {'uri': ''}).get('uri').split('/')[-1],
        'sameAsId': str(tree.get('same_as', dummy).id),
        'sameAsParentIds': parent_ids(tree.get('same_as')),
        'enrichedSameAsName': tree.get('enriched_same_as', {}).get('skos__prefLabel') or tree.get('enriched_same_as', {}).get('rdfs__label') or tree.get('enriched_same_as', {'uri': ''}).get('uri').split('/')[-1],
        'enrichedSameAsId': str(tree.get('enriched_same_as', dummy).id),
        'enrichedSameAsParentIds': parent_ids(tree.get('enriched_same_as', dummy))
    }

    if tree.get('children'):
        json['children'] = [tree_to_json(x) for x in tree['children']]

    if tree.get('subjects'):
        json['subjects'] = tree_to_json(tree['subjects'])

    return json
