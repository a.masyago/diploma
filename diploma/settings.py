import os

HOST = os.getenv('HOST', 'localhost')
PORT = int(os.getenv('POST', '7070'))
CLIENT_MAX_SIZE = (1024**2) * 5

SQLITE_FOLDER = os.getenv('SQLITE_FOLDER')
NEO4J_ADDRESS = os.getenv('NEO4J_ADDRESS', 'bolt://localhost:7687')
NEO4J_USER = os.getenv('NEO4J_USER', 'neo4j')
NEO4J_PASSWORD = os.getenv('NEO4J_PASSWORD', 'test')
NEO4J_IMPORT_LOCATION = os.getenv('NEO4J_IMPORT_LOCATION', '/home/andrey/neo4j/import')
