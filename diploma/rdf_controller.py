from neo4j import GraphDatabase


class RdfController:
    def __init__(self, uri, user, password):
        self.driver = GraphDatabase.driver(uri, auth=(user, password))

    def close(self):
        self.driver.close()

    def search_concept(self, search):
        with self.driver.session() as session:
            return session.read_transaction(self._search_concept, search)

    @staticmethod
    def _search_concept(tx, search):
        result = tx.run('''
MATCH (ru:skos__Concept) -[ru_en_link:owl__sameAs]-> (en:skos__Concept)
WHERE ru.skos__prefLabel STARTS WITH $searchKey
OPTIONAL MATCH (ru_child:skos__Concept) -[ru_child_link:skos__broader]-> (ru)
OPTIONAL MATCH (ru) -[ru_parent_link:skos__broader]-> (ru_parent:skos__Concept)
OPTIONAL MATCH (en) -[en_parent_link:skos__broader]-> (en_parent:skos__Concept) <-[:owl__sameAs]- (ru_parent)
OPTIONAL MATCH (en) <-[en_child_link:skos__broader]- (en_child:skos__Concept) <-[:owl__sameAs]- (ru_child)
RETURN ru_child, ru_child_link, ru, ru_parent_link, ru_parent, ru_en_link, en_child, en_child_link, en, en_parent_link, en_parent
LIMIT 1000
''', searchKey=search)

        return [record for record in result]

    def search_by_prefix(self, search):
        with self.driver.session() as session:
            return session.read_transaction(self._search_by_prefix, search)

    @staticmethod
    def _search_by_prefix(tx, search):
        result = tx.run('''
MATCH (en:skos__Concept)
WHERE toLower(en.skos__prefLabel) STARTS WITH toLower($searchKey)
RETURN DISTINCT id(en) as id, en.skos__prefLabel as label, en.uri as uri
ORDER by size(en.skos__prefLabel), en.skos__prefLabel
LIMIT 10
''', searchKey=search)

        return [record for record in result]

    # MATCH (n) -[:owl__sameAs]-> (m) WHERE id(n) = $id RETURN m
    def get_by_id(self, id):
        with self.driver.session() as session:
            return session.read_transaction(self._get_by_id, id)

    @staticmethod
    def _get_by_id(tx, id):
        result = tx.run('''
MATCH (n:skos__Concept) WHERE ID(n) = $searchId RETURN n
''', searchId=id)

        a = result.single()

        if not a:
            return a

        return a['n']

    # MATCH (n) -[:owl__sameAs]-> (m) WHERE id(n) = $id RETURN m
    def get_same_as(self, id):
        with self.driver.session() as session:
            return session.read_transaction(self._get_same_as, id)

    @staticmethod
    def _get_same_as(tx, id):
        result = tx.run('''
MATCH (self:skos__Concept) -[ru_en_link:owl__sameAs]-> (same_as:skos__Concept)
WHERE ID(self) = $searchId RETURN same_as
''', searchId=id)

        a = result.single()

        if not a:
            return a

        return a['same_as']

    # MATCH (n) -[:skos__broader]-> (m)
    # WHERE id(n) = $id
    # OPTIONAL MATCH (m) -[:owl__sameAs]-> (en)
    # RETURN m, en
    def get_parents(self, id):
        with self.driver.session() as session:
            return session.read_transaction(self._get_parents, id)

    @staticmethod
    def _get_parents(tx, id):
        result = tx.run('''
MATCH (self:skos__Concept) -[:skos__broader]-> (self_parent:skos__Concept)
WHERE id(self) = $searchId
OPTIONAL MATCH (self_parent:skos__Concept) -[ru_en_link:owl__sameAs]-> (self_parent_same_as:skos__Concept)
WHERE self_parent_same_as.uri STARTS WITH "http://wikidata"
RETURN self_parent, self_parent_same_as
''', searchId=id)

        return [(record['self_parent'], record['self_parent_same_as']) for record in result]

    # MATCH (n) <-[:skos__broader]- (m)
    # WHERE id(n) = $id
    # OPTIONAL MATCH (m) -[:owl__sameAs]-> (en)
    # RETURN m, en
    def get_children(self, id):
        with self.driver.session() as session:
            return session.read_transaction(self._get_children, id)

    @staticmethod
    def _get_children(tx, id):
        result = tx.run('''
MATCH (self:skos__Concept) <-[:skos__broader]- (self_child:skos__Concept)
WHERE id(self) = $searchId
OPTIONAL MATCH (self_child:skos__Concept) -[ru_en_link:owl__sameAs]-> (self_child_same_as:skos__Concept)
WHERE self_child_same_as.uri STARTS WITH "http://wikidata"
RETURN self_child, self_child_same_as
''', searchId=id)

        return [(record['self_child'], record['self_child_same_as']) for record in result]

    # MATCH (n) -[:skos__broader]-> (m)
    # WHERE id(n) = $same_as_id
    # OPTIONAL MATCH (m) <-[:owl__sameAs]- (ru)
    # RETURN m, ru
    def get_same_as_parents(self, same_as_id):
        with self.driver.session() as session:
            return session.read_transaction(self._get_same_as_parents, same_as_id)

    @staticmethod
    def _get_same_as_parents(tx, same_as_id):
        result = tx.run('''
MATCH (same_as:skos__Concept) -[:skos__broader]-> (same_as_parent:skos__Concept)
WHERE id(same_as) = $searchId
OPTIONAL MATCH (same_as_parent:skos__Concept) <-[ru_en_link:owl__sameAs]- (same_as_parent_same_as:skos__Concept)
WHERE same_as_parent.uri STARTS WITH "http://wikidata"
RETURN same_as_parent, same_as_parent_same_as
''', searchId=same_as_id)

        return [(record['same_as_parent'], record['same_as_parent_same_as']) for record in result]

    # MATCH (n) <-[:skos__broader]- (m)
    # WHERE id(n) = $same_as_id
    # OPTIONAL MATCH (m) -[:owl__sameAs]-> (ru)
    # RETURN m, ru
    def get_same_as_children(self, same_as_id):
        with self.driver.session() as session:
            return session.read_transaction(self._get_same_as_children, same_as_id)

    @staticmethod
    def _get_same_as_children(tx, same_as_id):
        result = tx.run('''
MATCH (same_as:skos__Concept) <-[:skos__broader]- (same_as_child:skos__Concept)
WHERE id(same_as) = $searchId
OPTIONAL MATCH (same_as_child:skos__Concept) <-[ru_en_link:owl__sameAs]- (same_as_child_same_as:skos__Concept)
WHERE same_as_child.uri STARTS WITH "http://wikidata"
RETURN same_as_child, same_as_child_same_as
''', searchId=same_as_id)

        return [(record['same_as_child'], record['same_as_child_same_as']) for record in result]

    def get_all_children(self, list_of_categories):
        with self.driver.session() as session:
            return session.read_transaction(self._get_all_children, list_of_categories)

    @staticmethod
    def _get_all_children(tx, list_of_categories):
        result = tx.run('''
MATCH (n:skos__Concept) -[:skos__broader]-> (m:skos__Concept)
WHERE m.uri IN $listOfCategories
RETURN n.uri, n.skos__prefLabel
''', listOfCategories=list_of_categories)
        return [(record[0], record[1]) for record in result]

    def get_same_as_and_children_of_same_as(self, target_node_uri):
        with self.driver.session() as session:
            return session.read_transaction(self._get_same_as_and_children_of_same_as, target_node_uri)

    @staticmethod
    def _get_same_as_and_children_of_same_as(tx, target_node_uri):
        result = tx.run('''
MATCH (target:skos__Concept) <-[same_as_link:owl__sameAs]- (source:skos__Concept)
WHERE target.uri = $targetNodeUri
OPTIONAL MATCH (source) <-[source_broader_link:skos__broader]- (source_child:skos__Concept)
RETURN same_as_link, source, source_broader_link, source_child
''', targetNodeUri=target_node_uri)

        return [(record['same_as_link'], record['source'], record['source_broader_link'], record['source_child']) for record in result]

    def get_children_by_uri(self, target_node_uri):
        with self.driver.session() as session:
            return session.read_transaction(self._get_children_by_uri, target_node_uri)

    @staticmethod
    def _get_children_by_uri(tx, target_node_uri):
        result = tx.run('''
MATCH (self:skos__Concept) <-[:skos__broader]- (self_child:skos__Concept)
WHERE self.uri = $targetNodeUri
RETURN self_child
''', targetNodeUri=target_node_uri)

        return [(record) for record in result]

    def get_by_uri(self, target_node_uri):
        with self.driver.session() as session:
            return session.read_transaction(self._get_by_uri, target_node_uri)

    @staticmethod
    def _get_by_uri(tx, target_node_uri):
        result = tx.run('''
MATCH (self:skos__Concept)
WHERE self.uri = $targetNodeUri
RETURN self
''', targetNodeUri=target_node_uri)

        a = result.single()

        if not a:
            return a

        return a['self']

    def get_children_path_by_uri(self, target_node_uri):
        with self.driver.session() as session:
            return session.read_transaction(self._get_children_path_by_uri, target_node_uri)

    @staticmethod
    def _flatten_layer(layer):
        result = []
        for node in layer.values():
            node['children'] = RdfController._flatten_layer(node['children'])

            result.append(node)

        return result

    @staticmethod
    def _flatten_tree(start_node, tree):
        return {
            'node': start_node,
            'children': RdfController._flatten_layer(tree)
        }

    @staticmethod
    def _get_children_path_by_uri(tx, target_node_uri):
        result = tx.run('''
MATCH p = (self:Resource) <-[:skos__broader*1..2]- (child:skos__Concept)
WHERE self.uri = $targetNodeUri
RETURN p
''', targetNodeUri=target_node_uri)

        tree = {}
        start_node = None

        for (path, ) in result:
            if path.start_node.get('uri', '') != target_node_uri:
                print(f'unknown start node: {path.start_node}')
                continue
            elif not start_node:
                start_node = path.start_node

            subtree = tree
            for segment in path.relationships:
                if segment.start_node.id not in subtree:
                    subtree[segment.start_node.id] = {
                        'node': segment.start_node,
                        'children': {}
                    }

                subtree = subtree[segment.start_node.id]['children']

        tree = RdfController._flatten_tree(start_node, tree)

        return tree

    def get_same_as_and_children_of_same_as_many(self, target_node_uris, link_name='sameAs'):
        with self.driver.session() as session:
            return session.read_transaction(self._get_same_as_and_children_of_same_as_many, target_node_uris, link_name)

    @staticmethod
    def _get_same_as_and_children_of_same_as_many(tx, target_node_uris, link_name):
        result = tx.run(f'''
MATCH (target:Resource) <-[:owl__{link_name}]- (source:skos__Concept)
WHERE target.uri IN $targetNodeUris
OPTIONAL MATCH (source:skos__Concept) <-[:skos__broader]- (child:skos__Concept)
RETURN target, source, child
''', targetNodeUris=target_node_uris)

        return [(record[0], record[1], record[2]) for record in result]

    def get_subjects_many(self, target_node_uris):
        with self.driver.session() as session:
            return session.read_transaction(self._get_subjects_many, target_node_uris)

    @staticmethod
    def _get_subjects_many(tx, target_node_uris):
        result = tx.run('''
MATCH (category:Resource) <-[:dct__subject]- (subject:Resource)
WHERE category.uri IN $targetNodeUris
RETURN category.uri, subject
''', targetNodeUris=target_node_uris)

        return [(record[0], record[1]) for record in result]

    def get_subjects_many_with_same_as(self, target_node_uris, link_name='sameAs'):
        with self.driver.session() as session:
            return session.read_transaction(self._get_subjects_many_with_same_as, target_node_uris, link_name=link_name)

    @staticmethod
    def _get_subjects_many_with_same_as(tx, target_node_uris, link_name='sameAs'):
        result = tx.run(f'''
MATCH (category:Resource) <-[:dct__subject]- (subject:Resource) -[:owl__{link_name}]-> (sameAs:Resource)
WHERE category.uri IN $targetNodeUris
RETURN category.uri, subject, sameAs.uri
''', targetNodeUris=target_node_uris)

        results_with_same_as = [(record[0], record[1], record[2]) for record in result]

        result = tx.run(f'''
MATCH (category:Resource) <-[:dct__subject]- (subject:Resource)
WHERE category.uri IN $targetNodeUris and not (subject) -[:owl__{link_name}]-> (:Resource)
RETURN category.uri, subject
        ''', targetNodeUris=target_node_uris)

        results_without_same_as = [(record[0], record[1]) for record in result]

        return [results_with_same_as, results_without_same_as]

    def import_relations(self, fname):
        with self.driver.session() as session:
            return session.write_transaction(self._import_relations, fname)

    @staticmethod
    def _import_relations(tx, fname):
        tx.run(f'''
CALL n10s.rdf.import.fetch("file:///var/lib/neo4j/import/{fname}","Turtle")
        ''')

        return None
