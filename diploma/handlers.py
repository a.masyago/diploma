import json
import os

from aiohttp import web

from diploma.db import load_relations, add_relation
from diploma.tidy_tree import build_tree, tree_to_json
from diploma import settings


async def healthcheck(_):
    return web.Response(body='ok', content_type='text/plain')


async def get_concept(request):
    search = request.query.get('search')

    if not search:
        return web.json_response({'error': '`search` query parameter is required.'}, status=400)

    rdf = request.app.rdf

    result = rdf.search_concept(search)

    response = []
    for record in result:
        response.append({
            'ru_child': (record['ru_child'] or {}).get('skos__prefLabel'),
            'ru': (record['ru'] or {}).get('skos__prefLabel'),
            'ru_parent': (record['ru_parent'] or {}).get('skos__prefLabel'),
            'en_child': (record['en_child'] or {}).get('skos__prefLabel'),
            'en': (record['en'] or {}).get('skos__prefLabel'),
            'en_parent': (record['en_parent'] or {}).get('skos__prefLabel'),
        })

    return web.json_response(response, dumps=lambda x: json.dumps(x, indent=2, ensure_ascii=False))


# TODO: request.query.search => {'label': <skos__prefLabel>, 'id': <id>}, only russian, only with sameAs link
async def search(request):
    search = request.query.get('search')

    if not search:
        return web.json_response({'error': '`search` query parameter is required.'}, status=400)

    rdf = request.app.rdf

    result = rdf.search_by_prefix(search)

    response = []
    for record in result:
        response.append({
            'id': (record or {}).get('id'),
            'uri': (record or {}).get('uri'),
            'name': (record or {}).get('label'),
        })

    return web.json_response(response, dumps=lambda x: json.dumps(x, indent=2, ensure_ascii=False))


def node_to_json(node, props=None):
    if not node:
        return node

    if not props:
        props = {}

    return {
        'id': node.id,
        'name': node.get('skos__prefLabel'),
        **props
    }


def sort_unmatched(unmatched):
    # unmatched = [(concept, sameAs), ...]
    # a - unnmatched with sameAs is nor None
    # b - unmatched with same is None
    a = [x for x in unmatched if x[1]]
    b = [x for x in unmatched if not x[1]]

    return a + b


def get_matched_unmatched(concept_list, same_as_list):
    concept_ids = set(x[0].id for x in concept_list)
    same_as_ids = set(x[0].id for x in same_as_list)

    matched = [x for x in concept_list if x[1] and x[1].id in same_as_ids]

    concept_unmatched = [x for x in concept_list if not x[1] or x[1].id not in same_as_ids]

    same_as_unmatched = [x for x in same_as_list if not x[1] or x[1].id not in concept_ids]

    return matched, sort_unmatched(concept_unmatched), sort_unmatched(same_as_unmatched)


async def get_info(request):
    concept_id = request.query.get('id')

    try:
        concept_id = int(concept_id)
    except ValueError:
        return web.json_response({'error': '`id` must be an integer.'}, status=400)

    rdf = request.app.rdf

    concept = rdf.get_by_id(concept_id)
    same_as = rdf.get_same_as(concept_id)

    concept_parents = rdf.get_parents(concept_id)
    same_as_parents = rdf.get_same_as_parents(same_as.id)

    matched_parents, concept_unmatched_parents, same_as_unmatched_parents = get_matched_unmatched(concept_parents, same_as_parents)
    matched_parents = [node_to_json(x[0], {'sameAs': node_to_json(x[1])}) for x in matched_parents]
    concept_unmatched_parents = [node_to_json(x[0], {'sameAs': node_to_json(x[1])}) for x in concept_unmatched_parents]
    same_as_unmatched_parents = [node_to_json(x[0], {'sameAs': node_to_json(x[1])}) for x in same_as_unmatched_parents]

    concept_children = rdf.get_children(concept_id)
    same_as_children = rdf.get_same_as_children(same_as.id)

    matched_children, concept_unmatched_children, same_as_unmatched_children = get_matched_unmatched(concept_children, same_as_children)
    matched_children = [node_to_json(x[0], {'sameAs': node_to_json(x[1])}) for x in matched_children]
    concept_unmatched_children = [node_to_json(x[0], {'sameAs': node_to_json(x[1])}) for x in concept_unmatched_children]
    same_as_unmatched_children = [node_to_json(x[0], {'sameAs': node_to_json(x[1])}) for x in same_as_unmatched_children]

    result = node_to_json(concept, {
        'matchedParents': matched_parents,
        'matchedChildren': matched_children,
        'unmatchedParents': concept_unmatched_parents,
        'unmatchedChildren': concept_unmatched_children,
        'sameAs': node_to_json(same_as, {
            'unmatchedParents': same_as_unmatched_parents,
            'unmatchedChildren': same_as_unmatched_children
        }),
    })

    return web.json_response(result, dumps=lambda x: json.dumps(x, indent=2, ensure_ascii=False))


async def get_tidy_tree(request):
    rdf = request.app.rdf

    target_uri = request.query.get('uri')
    if not target_uri:
        target_uri = "http://wikidata.dbpedia.org/resource/Q1458083"

    relations = request.query.get('relations')
    if not relations:
        relations = ['', '']
    else:
        relations = relations.split(',')
        if len(relations) == 1:
            relations += ['']
        relations = relations[:2]

    tree = build_tree(rdf, target_uri, relations)

    serialized = tree_to_json(tree)

    return web.json_response(serialized, dumps=lambda x: json.dumps(x, indent=2, ensure_ascii=False))


async def get_relations(request):
    relations = load_relations()

    return web.json_response(relations, dumps=lambda x: json.dumps(x, indent=2, ensure_ascii=False))


async def post_relations(request):
    data = await request.post()
    name = data['name']
    relation = data['relation']
    ttl = data['ttl']

    if not ttl or not name or not relation:
        relations = load_relations()
        return web.json_response(relations, dumps=lambda x: json.dumps(x, indent=2, ensure_ascii=False))

    fname = f'{relation}.ttl'
    tmp_path = os.path.join(settings.NEO4J_IMPORT_LOCATION, fname)

    with open(tmp_path, 'w+') as f:
        for line in ttl.file:
            line = line.decode('utf-8')
            chunks = line.split("> <")
            r_chunk = chunks[1]
            r_chunk = ("/".join(r_chunk.split('/')[:-1])) + '/owl#' + relation
            line = "> <".join([chunks[0], r_chunk, chunks[2]])
            f.write(line)

    rdf = request.app.rdf
    rdf.import_relations(fname)

    os.remove(tmp_path)

    relations = load_relations()
    already_saved = False
    for r in relations:
        if r['relation'] == relation:
            already_saved = True
            break

    if not already_saved:
        add_relation(name, relation)
        relations = load_relations()

    return web.json_response(relations, dumps=lambda x: json.dumps(x, indent=2, ensure_ascii=False))
